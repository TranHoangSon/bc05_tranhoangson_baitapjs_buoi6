// ex_1
function tinhGiaTri() {
  var numberX = document.getElementById("txt-x").value * 1;
  var numberN = document.getElementById("txt-n").value * 1;
  var total = 0;
  for (var i = 1; i <= numberN; i++) {
    total = total + Math.pow(numberX, i);
  }
  document.getElementById("result").innerText = `Tong=  ${total}`;
}
// end ex_1

// ex_2
function tinhGiaiThua() {
  var number = document.querySelector("#txt-number").value * 1;
  var sum = 1;
  for (var i = 1; i <= number; i++) {
    sum = sum * i;
  }
  document.querySelector("#result1").innerHTML = `Giai Thua: ${sum}`;
}
// ex_2

// ex_3
function timSoNguyen() {
  var sum1 = 0;
  var soNhoNhat = 0;
  for (var i = 0; i < 10000; i++) {
    sum1 += i;
    if (sum1 > 10000) {
      soNhoNhat = i;
      break;
    }
  }

  document.getElementById(
    "result2"
  ).innerHTML = `So Nguyen Duong Nho Nhat: ${soNhoNhat}`;
}
// ex_4

function taoDiv() {
  var content = ``;
  for (var i = 0; i <= 10; i++) {
    if (i % 2 == 0) {
      content += `<div class="bg-danger">Div chan</div>`;
    } else {
      content += `<div class="bg-success">Div le</div>`;
    }
  }
  document.getElementById("print").innerHTML = content;
}
